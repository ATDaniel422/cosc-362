#include <stdio.h>
#include <stdlib.h>

/*   This is a C program to create a red and blue plain pixel map.
 *   Usage: 
 *    
 *   RedBluePPM outfileName numRows numCols
 *   
 *   Here our program takes in an outfile name and two arguments the rows and columns for the size of the image to produce
 *   
 *   To Run: gcc RedBluePPM.c -o RedBluePPM
 *   
 */

int main(int argc,      // This is the number of things passed to the function 
         char *argv[]   // This is the array of things passed
        ){

    int numRows;   // Placeholder for number of rows
    int numCols;   // Placeholder for number of columns
    int imageSize;
    int row;
    int col;
    unsigned char *outImage; // pixel pointer
    unsigned char *ptr;      // a pointer 
    //unsigned char *outputFP; // Output file pointer
    FILE *outputFP;

    if(argc!=4) {
        printf("Usage: ./RedBluePPM outfileName numrow numcols \n\n");
        exit(1);
    }
    if ((numRows = atoi(argv[2])) <= 0) {
        printf("Error: numRows needs to be positive");
    }
    
    if ( (numCols = atoi(argv[3]) ) <= 0) {
        printf("Error: numCols needs to be positive");
    }

    //===========================================
    // Set up space for my soon to be ppm image.
    //===========================================

    imageSize = numRows * numCols * 3;
    outImage = (unsigned char *) malloc(imageSize); //get enough space for my image.

    // Open a file to put the output image into
    if ((outputFP = fopen(argv[1], "w")) == NULL) {
        perror("output open error");
        printf("Error: cannot open output file\n");
        exit(1);
    }

    // Now let's create the plain pixel map
    ptr = outImage;
    for(row=0; row< numRows; row++) {
        for(col = 0; col < numCols; col++) {
            // We walk through each row of the image column by column.
            if (col < numCols/2) {
                // Red pixel
                *ptr = 255;
                *(ptr+1) = 0;
                *(ptr+2) = 0;
            }
            else {
                // Blue pixel
                *ptr = 0;
                *(ptr+1) = 0;
                *(ptr+2) = 255;
            }
            // Advance the pointer
            *ptr += 3;
        }
    }
    // Put all of this information into a file wiht a need header.
    fprintf(outputFP, "P6 %d %d 255\n", numCols, numRows);
    fwrite(outImage, 1, imageSize, outputFP);

    // Done
    fclose(outputFP);
    
    return 0;
}
