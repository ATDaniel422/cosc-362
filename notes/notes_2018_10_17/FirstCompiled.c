#include <stdio.h>
#include <stdlib.h>

/*   This is a C program that does something cool.
 *   I'm not sure what it is yet.
 *
 *   To Run: gcc FirstCompiled.c -o MyProgram
 *   Then: ./MyProgram
 */

int main(int argc,      // This is the number of things passed to the function 
         char *argv[]   // This is the array of things passed
        ){

    printf("===========\n"); 
    printf("Hello World\n");    //this is a sigle-line comment.
    printf("===========\n\n");
    
    return 0;
}
