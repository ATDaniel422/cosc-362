#!/bin/bash

# Hopefully we will read some parameters from the command line for this script

echo "To use: ./argumentsExample.sh Thing1 Thing2 Thing3"

POSPAR1="$1"
POSPAR2="$2"
POSPAR3="$3"

echo "$1 is the first position parameter. \$1"
echo "$2 is the second position parameter. \$2"
echo "$2 is the third position parameter. \$3"
echo
echo "The total number of parameters is given by $#."
