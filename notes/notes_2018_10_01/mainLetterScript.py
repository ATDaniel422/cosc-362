import sys
from LetterFunctions import *

""" 
    This is a script to do a mail merge to make custom letters to mythical beings. 
    To run: $ python3 mainLetterSender.py namesAndAddresses.txt
"""

#################################################
# Read in command-line args
#################################################


if len(sys.argv) != 2:
    print("To run: python3 mainLetterScript.py namesAndAddresses.txt")
else:
    print('Using addresses form the file', str(sys.argv[1]))
    dataFile = str(sys.argv[1])

addressData = open(dataFile, 'r')

print("Reading form a file " + str(sys.argv[1]))

AddressList = []
for line in addressData.readlines():
    First, Last, Addr1, Addr2 = map(str, line.split(','))
    CurrentAddress = Address(First, Last, Addr1, Addr2)
    AddressList.append(CurrentAddress)
    CurrentAddress.dumpAddress()

for i in range(len(AddressList)):
    Letter = DocumentText(AddressList[i])
    Letter.writeLetter()


addressData.close()
