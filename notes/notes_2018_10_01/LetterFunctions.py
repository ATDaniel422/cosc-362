import sys
import os

"""
    this is my library of functions that can be called in the letter writing main
"""

# Defining a class
class Address:
    
    def __init__(self, first, last, addr1, addr2):
        self.first = first
        self.last  = last
        self.addr1 = addr1
        self.addr2 = addr2
    
    def dumpAddress(self):
        print("\nAddress Info: ")
        print("First: ", self.first.strip())
        print("Last:  ", self.last.strip())
        print("Addr1: ", self.addr1.strip())
        print("Addr2: ", self.addr2.strip())

class DocumentText:

    def __init__(self, address):
        self.address = address

        self.header = ("\\documentclass[12pt]{article} \n"
                        + "\\usepackage{geometry} \n"
                        + "\\geometry{hmargin={1in,1in},vmargin={2in,1in}}"
                        + "\\being{document} \n"
                        + "\\thispagestyle{empty} \n\n")

        self.myAddress = "My Address \n\n\\vskip.5in \n\n"

        self.toAddress = (self.address.first + " " + self.address.last + "\n\n" 
                          + self.address.addr1 + "\n\n"
                          + self.address.addr2 + "\n\n")
        
        self.date = "\\vskip.5in \n \\today \n\n \\vskip.5in \n\n"
        
        self.greeting = "Dear" + self.address.first + " " + self.address.last + ", \n\n \\vskip.5in"

        self.body = ("I would like to thank you for being awesome and adding so much magin to the world."
                    + " Please don't have Darth Vader come to my house and look at my files."
                    + " Let's all get together soon."
                    + "\n\n \\vskip.5in \n \\hskip3.5in Your Friend," 
                    + "\\hskip3.5in Drew Daniel")

        self.footer = "\\end{document}"
    
    def writeLetter(self):
        LetterOutName = str(self.address.last.lstrip()) + ".tex"
        LetterFile = open(LetterOutName, "w")
        LetterFile.write(self.header)
        LetterFile.write(self.myAddress)
        LetterFile.write(self.toAddress)
        LetterFile.write(self.date)
        LetterFile.write(self.greeting)
        LetterFile.write(self.body)
        LetterFile.write(self.footer)
        LetterFile.close()
