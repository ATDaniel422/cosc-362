/* This is a bunch of stuff to use in pixel mapping. */
#include <math.h>   // Allows me to use power funcitons: pow(x,y) -> x^y

int InCircle(int totalrows, int totalcols, int radius, int pixRow, int pixCol) {
    int InOrOut = 0;    // Integer flag for if I'm in the circle or not.
                        // 1 is yes, 0 is no. Default to no
    int Centerx = totalcols/2;
    int Centery = totalrows/2;
    int dist    = 0;        //Distance from center to pixel
    
    dist = pow((pow(Centerx-pixCol,2))+(pow(Centery - pixRow,2)),0.5);

    if (dist > radius) {
        InOrOut = 1;
    }
    // else, it's zero, which we set it to by default

    return InOrOut;
}

int InStem(int totalrows, int totalcols, int radius, int pixRow, int pixCol) {
    int InOrOut = 0;    // Integer flag for being in the tem
                        // 2 = yes, 0 = no
    int Centerx = totalcols/2;
    int Centery = totalrows/2;
    int topY = Centery + radius;
    int dist = 0; 
}
