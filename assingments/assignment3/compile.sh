#!/bin/bash

for i in $( ls -R *.tex ):
do
    pdflatex $i
    rm *.aux
    rm *.log
    rm *.bib

    echo $i >> ~/LatexCompileReport.txt
    wc $i >> ~/LatexCompileReport.txt
    echo "\n" >> ~/LatexCompileReport.txt
done


