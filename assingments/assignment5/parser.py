import os

# Usage: place this file in the directory with your individual timesheets
#        enter "python3 parser.py"
# 
#        Timesheet files should be of the type .txt
# 
# Note: will not work on anything older than Python 3.6, only python 3.6.* and 3.7.* are supported

# Create list of .txt files, ignoring the file produced by this script
my_files = os.listdir('.')
time_sheets = []
for i in range(len(my_files)):
    if my_files[i].endswith('.txt') and my_files[i] != "time_report.txt":
        time_sheets.append(my_files[i])

# Create target file; if file of the same name exists, overwrite it 
with open('time_report.txt', 'w') as time_report:

    # Put Heading on page
    time_report.write("Drew Daniel \nCOSC-362 \nTime log sheet\n\n")
    
    # Parse through all of the files collecting data
    for files in time_sheets:
        total_time = 0
        name = ""

        # open each file, read it and break it up into lines
        with open(files, 'r') as current_file:
            full_text = current_file.read()
            file_lines = full_text.split('\n')
            file_lines.remove('')
            name = file_lines[0]

            # Get minutes and hours from each line
            for line in range(1, len(file_lines)):
                words = file_lines[line]
                words = words.split()
                hours = int(words[1][:-2])
                minutes = int(words[2][:-3])
                total_time += minutes + (hours * 60)
        
        # Restore total_time to hours/minutes
        total_hours = int(total_time / 60)
        total_minutes = total_time % 60

        # Add person's data to the time_report file
        time_report.write(f'{name.ljust(20)} {total_hours:02}hr  {total_minutes:02}min \n')
